<?php

/**
 * Class TemplateManager
 */
class TemplateManager
{
    /**
     * @param Template $tpl
     * @param array $data
     * @throws RuntimeException
     * @return Template
     */
    public function getTemplateComputed(Template $tpl, array $data)
    {
        if (!$tpl) {
            throw new \RuntimeException('no tpl given');
        }

        $replaced = clone($tpl);
        $replaced->subject = $this->computeText($replaced->subject, $data);
        $replaced->content = $this->computeText($replaced->content, $data);

        return $replaced;
    }

    /**
     * @param string $text
     * @param array $data
     * @throws RuntimeException
     * @return string
     */
    private function computeText($text, array $data)
    {
        # retrieve objects
        $quote = (isset($data['quote']) && $data['quote'] instanceof Quote) ? $data['quote'] : null;

        if($quote === null)
            throw new \RuntimeException('Unable to retrieve Quote object');

        $user  = (isset($data['user']) && ($data['user'] instanceof User))  ? $data['user'] : ApplicationContext::getInstance()->getCurrentUser();
        $site = SiteRepository::getInstance()->getById($quote->siteId);
        $destination = DestinationRepository::getInstance()->getById($quote->destinationId);

        # extract placeholders from text
        $matchesCount = preg_match_all("/[\[]+([\w]+)+[:]+([\w]+)+[]]/m", $text, $matches);

        # if no placeholders return text
        if($matchesCount === 0)
            return $text;

        # process loop over each regex match
        foreach ($matches[0] as $index => $match)
        {
            # match objects are in the first regex capturing group $matches[1]
            $matchObject = $matches[1][$index];

            # match properties are in the second regex capturing group $matches[2]
            $matchProperty = $matches[2][$index];

            # process switch over each match objects
            switch ($matchObject)
            {
                case 'quote':

                    # process switch over each match property from quote objects
                    switch ($matchProperty)
                    {
                        case 'summary_html':
                            $text = str_replace('[quote:summary_html]', Quote::renderHtml($quote), $text);
                            break;

                        case 'summary':
                            $text = str_replace('[quote:summary]', Quote::renderText($quote), $text);
                            break;

                        case 'destination_name':
                            $text = str_replace('[quote:destination_name]',$destination->countryName,$text);
                            break;

                        case 'destination_link':
                            $text = str_replace('[quote:destination_link]', $site->url . '/' . $destination->countryName . '/quote/' . $quote->id, $text);
                            break;

                        # unknow property -> throw exception
                        default:
                            $this->throwPropertyNotFoundException($matchObject, $matchProperty);
                    }

                    break;

                case 'user':

                    # process switch over each match property from user objects
                    switch ($matchProperty)
                    {
                        case 'first_name':
                            $text = str_replace('[user:first_name]', ucfirst(mb_strtolower($user->firstname)), $text);
                            break;

                        # unknow property -> throw exception
                        default:
                            $this->throwPropertyNotFoundException($matchObject, $matchProperty);
                    }

                    break;

                # unknow object -> throw exception
                default:
                    throw new \RuntimeException("'$matchObject' object not supported");
            }
        }

        return $text;
    }

    /**
     * @param string $matchObject
     * @param string $matchProperty
     */
    private function throwPropertyNotFoundException($matchObject, $matchProperty)
    {
        throw new \RuntimeException("'$matchProperty' property in '$matchObject' object not supported");
    }
}